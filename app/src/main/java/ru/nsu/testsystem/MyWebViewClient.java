package ru.nsu.testsystem;

import static ru.nsu.testsystem.MainActivity.TEST_SYSTEM_HOST;

import android.annotation.TargetApi;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.util.logging.Logger;

public class MyWebViewClient extends WebViewClient {

    Logger logger = Logger.getLogger(this.getClass().toString());

    @TargetApi(Build.VERSION_CODES.N)
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
        if (Uri.parse(request.getUrl().toString()).getHost().equals(TEST_SYSTEM_HOST)) {
            return false;
        }
        view.loadUrl(request.getUrl().toString());
        return true;
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (Uri.parse(url).getHost().equals(TEST_SYSTEM_HOST)) {
            return false;
        }
        view.loadUrl(url);
        return true;
    }

    @Override
    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        logger.warning("ssl error");
        logger.warning(error.getCertificate().toString());
        handler.proceed();
//         ignore ssl error
//        if (handler != null){
//            handler.proceed();
//        } else {
//            super.onReceivedSslError(view, null, error);
//        }
//        handler.cancel();
    }

}
